//
//  AppDelegate.m
//  Monitor
//
//  Created by Josep Dols on 21/4/15.
//  Copyright (c) 2015 Josep Dols. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    // Insert code here to initialize your application
}

- (void)applicationWillTerminate:(NSNotification *)aNotification {
    // Insert code here to tear down your application
}

@end
