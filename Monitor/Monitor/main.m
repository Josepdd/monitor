//
//  main.m
//  Monitor
//
//  Created by Josep Dols on 21/4/15.
//  Copyright (c) 2015 Josep Dols. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
    return NSApplicationMain(argc, argv);
}
