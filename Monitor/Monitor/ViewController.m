//
//  ViewController.m
//  Monitor
//
//  Created by Josep Dols on 21/4/15.
//  Copyright (c) 2015 Josep Dols. All rights reserved.
//

#import "ViewController.h"

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    // Do any additional setup after loading the view.
}

- (void)setRepresentedObject:(id)representedObject {
    [super setRepresentedObject:representedObject];

    // Update the view, if already loaded.
}

@end
